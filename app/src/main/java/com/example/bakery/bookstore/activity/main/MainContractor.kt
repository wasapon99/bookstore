package com.example.bakery.bookstore.activity.main

import com.example.bakery.bookstore.common.base.BaseMvpContractorPresenter
import com.example.bakery.bookstore.common.base.BaseMvpContractorView

class MainContractor {

    interface Presenter : BaseMvpContractorPresenter {

    }

    interface View : BaseMvpContractorView<Presenter> {

    }
}