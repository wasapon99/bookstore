package com.example.bakery.bookstore.util;

import android.app.Activity;
import android.content.Context;

import com.example.bakery.bookstore.manager.ContextManager;

public class ActivityUtils {

    private static ActivityUtils instance;
    private Context context;


    public static ActivityUtils getInstance() {
        if (instance == null) {
            instance = new ActivityUtils();
        }
        return instance;
    }

    private ActivityUtils() {
        context = ContextManager.getInstance().getContext();
    }

    public Activity getActivity() {
        return new Activity();
    }
}
