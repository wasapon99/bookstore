package com.example.bakery.bookstore.activity.login

import android.support.v4.content.ContextCompat.startActivity
import com.example.bakery.bookstore.common.base.BaseMvpPresenter
import com.example.bakery.bookstore.util.ActivityUtils
import com.google.firebase.auth.FirebaseAuth

class LoginPresenter(view: LoginContractor.View?) : BaseMvpPresenter<LoginContractor.View>(view), LoginContractor.Presenter {
    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var status: Boolean = true

    override fun start() {

    }

    override fun stop() {

    }

    fun setPresenter(view: LoginContractor.View): LoginPresenter {
        return LoginPresenter(view)
    }

    override fun requestLogin(email: String, password: String) {
        if (!(email.isEmpty() && password.isEmpty())) {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(ActivityUtils.getInstance().activity) {
                        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(ActivityUtils.getInstance().activity) { task ->
                            if (task.isSuccessful) {
                                status = true

                                view.getStatus(status)
                            } else {
                                status = false

                                view.getStatus(status)
                            }
                        }

                    }
        } else {
            view.getError()
        }

    }

}