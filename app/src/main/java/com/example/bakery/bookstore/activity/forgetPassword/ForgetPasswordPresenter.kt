package com.example.bakery.bookstore.activity.forgetPassword

import com.example.bakery.bookstore.common.base.BaseMvpPresenter
import com.google.firebase.auth.FirebaseAuth

class ForgetPasswordPresenter(view: ForgetPasswordContractor.View) : BaseMvpPresenter<ForgetPasswordContractor.View>(view), ForgetPasswordContractor.Presenter {
    private val mAuth = FirebaseAuth.getInstance()
    var status:Boolean = true

    override fun start() {
    }

    override fun stop() {
    }

    fun createPresenter(view: ForgetPasswordContractor.View): ForgetPasswordPresenter {
        return ForgetPasswordPresenter(view)
    }

    override fun requestSendEmail(email: String) {
        if (!email.isEmpty()) {
            mAuth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    status = true
                    view.sendStatus(status)
                } else {
                    status = false
                    view.sendStatus(status)
                }
            }
        } else {
            view.sendError()
        }
    }
}