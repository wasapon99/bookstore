package com.example.bakery.bookstore.activity.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.bakery.bookstore.R
import com.example.bakery.bookstore.activity.login.LoginActivity
import com.example.bakery.bookstore.common.base.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseMvpActivity<RegisterContractor.Presenter>(), RegisterContractor.View {

    override fun createPresenter() {
        val registerPresenter = RegisterPresenter(this)
        registerPresenter.createPresenter(this)
    }

    override fun getLayoutView(): Int {
        return R.layout.activity_register
    }

    override fun bindView() {
    }

    override fun setupView() {
        btnRegister.setOnClickListener {
            sendRegister()
        }

        tvAlready.setOnClickListener{
            intent = (Intent(this,LoginActivity::class.java))
            startActivity(intent)
            overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_right)
        }
    }

    private fun sendRegister() {
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()
        val name = etName.text.toString()
        val tel = etPhone.text.toString()

        if (!(email.isEmpty() && password.isEmpty() && name.isEmpty() && tel.isEmpty())) {
            presenter.requestRegister(email,password,name,tel)
        }
    }

    override fun restoreArgument(extras: Bundle?) {
    }

    override fun initialize() {
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?) {
    }

    override fun restoreView() {
    }

    override fun saveInstanceState(outState: Bundle?) {
    }

    override fun getStatus(status: Boolean) {
        if (status) {
            intent = (Intent(this, LoginActivity::class.java))
            startActivity(intent)
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right)
        } else {
            Toast.makeText(this,"Register Error",Toast.LENGTH_SHORT).show()
        }

    }

}
