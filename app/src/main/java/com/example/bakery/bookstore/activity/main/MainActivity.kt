package com.example.bakery.bookstore.activity.main

import android.os.Bundle
import com.example.bakery.bookstore.R
import com.example.bakery.bookstore.common.base.BaseMvpActivity

class MainActivity : BaseMvpActivity<MainContractor.Presenter>(), MainContractor.View {
    private val mainPresenter = MainPresenter(this)

    override fun createPresenter() {
        mainPresenter.createPresenter(this)
    }

    override fun getLayoutView(): Int {
        return R.layout.activity_main
    }

    override fun bindView() {
    }

    override fun setupView() {
    }

    override fun restoreArgument(extras: Bundle?) {
    }

    override fun initialize() {
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?) {
    }

    override fun restoreView() {
    }

    override fun saveInstanceState(outState: Bundle?) {
    }


}
