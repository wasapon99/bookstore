package com.example.bakery.bookstore

import android.app.Application
import com.example.bakery.bookstore.manager.ContextManager

class MyApplication :Application() {

    override fun onCreate() {
        super.onCreate()

        ContextManager.getInstance().init(applicationContext)
    }

    override fun onTerminate() {
        super.onTerminate()
    }
}