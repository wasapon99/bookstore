package com.example.bakery.bookstore.activity.register

import com.example.bakery.bookstore.common.base.BaseMvpPresenter
import com.example.bakery.bookstore.util.ActivityUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegisterPresenter(view: RegisterContractor.View) : BaseMvpPresenter<RegisterContractor.View>(view), RegisterContractor.Presenter {
    private val mAuth = FirebaseAuth.getInstance()
    private lateinit var mDatabase: DatabaseReference
    private var status:Boolean = true

    override fun start() {

    }

    override fun stop() {
    }

    fun createPresenter(view: RegisterContractor.View): RegisterPresenter {
        return RegisterPresenter(view)
    }

    override fun requestRegister(email: String, password: String, name: String, tel: String) {
        mDatabase = FirebaseDatabase.getInstance().getReference("Users")

        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(ActivityUtils.getInstance().activity) { task ->
            if (task.isSuccessful) {
                status = true
                val user = mAuth.currentUser
                val uId = user!!.uid
                mDatabase.child(uId).child("Name").setValue(name)
                mDatabase.child(uId).child("Email").setValue(email)
                mDatabase.child(uId).child("phone").setValue(tel)
                view.getStatus(status)
            } else {
                status = false
                view.getStatus(status)
            }
        }
    }
}




