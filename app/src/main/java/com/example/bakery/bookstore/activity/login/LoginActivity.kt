package com.example.bakery.bookstore.activity.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.bakery.bookstore.R
import com.example.bakery.bookstore.activity.forgetPassword.ForgetPasswordActivity
import com.example.bakery.bookstore.activity.main.MainActivity
import com.example.bakery.bookstore.activity.register.RegisterActivity
import com.example.bakery.bookstore.common.base.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseMvpActivity<LoginContractor.Presenter>(),LoginContractor.View {

    override fun createPresenter() {
        val loginPresenter = LoginPresenter(this)
        loginPresenter.setPresenter(this)
    }

    override fun getLayoutView(): Int {
        return R.layout.activity_login
    }

    override fun bindView() {
    }

    override fun setupView() {
        btnRegister.setOnClickListener{
            intentRegister()
        }

        btnLogin.setOnClickListener {
            sendLogin()
        }

        tvForgetPassword.setOnClickListener {
            intentForgetPassword()
        }
    }

    private fun intentForgetPassword() {
        startActivity(Intent(this,ForgetPasswordActivity::class.java))

    }

    private fun sendLogin() {
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()

        presenter.requestLogin(email,password)

    }

    override fun getError() {
        Toast.makeText(this,"Please Insert Data",Toast.LENGTH_SHORT).show()

    }

    override fun getStatus(status: Boolean) {
        if (status) {
            startActivity(Intent(this, MainActivity::class.java))
            overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        } else {
            Toast.makeText(this,"Login Error",Toast.LENGTH_SHORT).show()
        }
    }

    override fun restoreArgument(extras: Bundle?) {
    }

    override fun initialize() {
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?) {
    }

    override fun restoreView() {
    }

    override fun saveInstanceState(outState: Bundle?) {
    }

    private fun intentRegister() {
        intent = (Intent(this, RegisterActivity::class.java))
        startActivity(intent)
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
        finish()
    }

}
