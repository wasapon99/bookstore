package com.example.bakery.bookstore.activity.forgetPassword

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.bakery.bookstore.R
import com.example.bakery.bookstore.activity.login.LoginActivity
import com.example.bakery.bookstore.common.base.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_forget_password.*

class ForgetPasswordActivity : BaseMvpActivity<ForgetPasswordContractor.Presenter>(), ForgetPasswordContractor.View{

    override fun createPresenter() {
        val forgetPasswordPresenter = ForgetPasswordPresenter(this)
        forgetPasswordPresenter.createPresenter(this)

    }

    override fun getLayoutView(): Int {
        return R.layout.activity_forget_password
    }

    override fun bindView() {
    }

    override fun setupView() {
        btnReset.setOnClickListener {
            sendEmail()
        }
    }

    private fun sendEmail() {
        val email = etEmail.text.toString().trim()
        presenter.requestSendEmail(email)
    }

    override fun restoreArgument(extras: Bundle?) {
    }

    override fun initialize() {
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?) {
    }

    override fun restoreView() {
    }

    override fun saveInstanceState(outState: Bundle?) {
    }

    override fun sendStatus(status: Boolean) {
        if (status) {
            Toast.makeText(this, "Send email Success", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this,LoginActivity::class.java))
            overridePendingTransition(R.anim.anim_slide_in_right,R.anim.anim_slide_out_right)
            finish()
        } else {
            Toast.makeText(this, "Send email Error", Toast.LENGTH_SHORT).show()
        }

    }

    override fun sendError() {
        Toast.makeText(this, "Please Insert Email", Toast.LENGTH_SHORT).show()

    }

}
