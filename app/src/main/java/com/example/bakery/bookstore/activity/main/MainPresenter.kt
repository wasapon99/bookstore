package com.example.bakery.bookstore.activity.main

import android.view.View
import com.example.bakery.bookstore.common.base.BaseMvpPresenter

class MainPresenter(view:MainContractor.View) : BaseMvpPresenter<MainContractor.View>(view),MainContractor.Presenter {

    override fun start() {
    }

    override fun stop() {
    }

    fun createPresenter(view: MainContractor.View): MainPresenter {
        return MainPresenter(view)
    }
}