package com.example.bakery.bookstore.activity.login

import com.example.bakery.bookstore.common.base.BaseMvpContractorPresenter
import com.example.bakery.bookstore.common.base.BaseMvpContractorView

class LoginContractor {

    interface Presenter : BaseMvpContractorPresenter {
        fun requestLogin(email: String, password: String)
    }

    interface View : BaseMvpContractorView<Presenter> {
        fun getStatus(status: Boolean)
        fun getError()
    }
}