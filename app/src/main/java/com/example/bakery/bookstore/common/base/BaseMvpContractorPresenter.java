package com.example.bakery.bookstore.common.base;

public interface BaseMvpContractorPresenter {
    void start();

    void stop();
}