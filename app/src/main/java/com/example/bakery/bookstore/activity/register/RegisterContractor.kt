package com.example.bakery.bookstore.activity.register

import com.example.bakery.bookstore.common.base.BaseMvpContractorPresenter
import com.example.bakery.bookstore.common.base.BaseMvpContractorView

class RegisterContractor {

    interface Presenter : BaseMvpContractorPresenter {
        fun requestRegister(email: String, password: String, name: String, tel: String)

    }

    interface View : BaseMvpContractorView<Presenter> {
        fun getStatus(status: Boolean)
    }
}