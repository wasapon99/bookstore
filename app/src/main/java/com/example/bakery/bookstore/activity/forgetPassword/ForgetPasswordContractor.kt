package com.example.bakery.bookstore.activity.forgetPassword

import com.example.bakery.bookstore.common.base.BaseMvpContractorPresenter
import com.example.bakery.bookstore.common.base.BaseMvpContractorView

class ForgetPasswordContractor {

    interface Presenter : BaseMvpContractorPresenter {
        fun requestSendEmail(email: String)

    }

    interface View : BaseMvpContractorView<Presenter> {
        fun sendStatus(status: Boolean)
        fun sendError()

    }
}